<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class LayoutLoadBeforeFrontend
 * @package Magenest\Reservation\Observer\Layout
 */
class Load implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $fullActionName = $observer->getEvent()->getFullActionName();
        /** @var  $layout \Magento\Framework\View\Layout */
        $layout = $observer->getEvent()->getLayout();
        $handler = '';
        if ($fullActionName == 'catalog_product_view') {
            $productId = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Framework\App\RequestInterface::class)->getParam("id");
            if ($productId) {
                $eventId = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get(\Magenest\Ticket\Helper\Event::class)->isEvent($productId);
                if ($eventId) {
                    $handler = 'catalog_product_view_ticket';
                }
            }
        }
        if ($handler) {
            $layout->getUpdate()->addHandle($handler);
        }
    }
}
