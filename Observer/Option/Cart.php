<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Option;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Cart
 * @package Magenest\Ticket\Observer\Option
 */
class Cart implements ObserverInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Ticket\Model\EventoptionFactory
     */
    protected $option;

    /**
     * @var \Magenest\Ticket\Model\EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $session;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magenest\Ticket\Model\EventDateFactory
     */
    protected $date;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * Cart constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Ticket\Model\EventoptionFactory $optionFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\EventDateFactory $dateFactory
     * @param \Magenest\Ticket\Model\EventSessionFactory $sessionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Ticket\Model\EventoptionFactory $optionFactory,
        \Magenest\Ticket\Model\EventoptionTypeFactory $optionTypeFactory,
        \Magenest\Ticket\Model\EventLocationFactory $locationFactory,
        \Magenest\Ticket\Model\EventDateFactory $dateFactory,
        \Magenest\Ticket\Model\EventSessionFactory $sessionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_productRepository = $productRepository;
        $this->option = $optionFactory;
        $this->optionType = $optionTypeFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->location = $locationFactory;
        $this->date = $dateFactory;
        $this->session = $sessionFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $item = $observer->getEvent()->getQuoteItem();
            $product = $item->getProduct();
            $productId = $product->getId();
            $data = $this->_request->getParams();
            $checkTypeProduct = $this->_productRepository->getById($productId)->getTypeId();
            if ($checkTypeProduct == 'ticket') {
                if (!empty($data['additional_options'])) {
                    $options = $data['additional_options'];
                    $additionalOptions = [];
                    if (!empty($data['additional_options']['ticket_price'])) {
                        if (@$options['checkbox'] && count($options['checkbox']) > 0) {
                            foreach ($options['checkbox'] as $checkboxId => $optionsCheckbox) {
                                $title = $this->getTitleOption($checkboxId);
                                $optionsCheckboxTitle = [];
                                foreach ($optionsCheckbox as $id => $optionCheckbox) {
                                    $optionsCheckboxTitle[] = $this->getValueOption($id);
                                }
                                $additionalOptions[] = array(
                                    'label' => $title,
                                    'value' => implode(",", $optionsCheckboxTitle),
                                );
                            }
                        }
                        if (@$options['radio'] && count($options['radio']) > 0) {
                            foreach ($options['radio'] as $radioId => $radioOptions) {
                                /** @var  \Magenest\Ticket\Model\Eventoption $optionModel */
                                $title = $this->getTitleOption($radioId);
                                $value = explode("_", $radioOptions);
                                $additionalOptions[] = array(
                                    'label' => $title,
                                    'value' => $this->getValueOption((int)$value[1]),
                                );
                            }
                        }
                        if (@$options['dropdown'] && count($options['dropdown']) > 0) {
                            foreach ($options['dropdown'] as $dropdownId => $dropdownOptions) {
                                /** @var  \Magenest\Ticket\Model\Eventoption $optionModel */
                                $title = $this->getTitleOption($dropdownId);
                                $value = explode("_", $dropdownOptions);
                                $additionalOptions[] = array(
                                    'label' => $title,
                                    'value' => $this->getValueOption((int)$value[1]),
                                );
                            }
                        }

                        $item->setOriginalCustomPrice($options['ticket_price']);
                    }
                    $locationId = '';
                    $dateId = '';
                    $sessionId = '';
                    if (isset($options['single']) && !empty($options['single'])) {
                        if (!empty($options['single']['ticket_location'])) {
                            $locationId = $options['single']['ticket_location'];
                        }
                        if (!empty($options['single']['ticket_date'])) {
                            $dateId = $options['single']['ticket_date'];
                        }
                        if (!empty($options['single']['ticket_session'])) {
                            $sessionId = $options['single']['ticket_session'];
                        }
                    } else {
                        if (isset($options['ticket_location']) && !empty($options['ticket_location'])) {
                            $locationId = $options['ticket_location'];
                        }

                        if (isset($options['ticket_date']) && !empty($options['ticket_date'])) {
                            $dateId = $options['ticket_date'];
                        }

                        if (isset($options['ticket_session']) && !empty($options['ticket_session'])) {
                            $sessionId = $options['ticket_session'];
                        }
                    }

                    if (!empty($locationId)) {
                        $location = $this->location->create()->load($locationId);

                        $additionalOptions[] = array(
                            'label' => 'Location',
                            'value' => $location->getLocationTitle()
                        );
                    }
                    if (!empty($dateId)) {
                        $date = $this->date->create()->load($dateId);
                        $start = 'no limit';
                        $end = 'no limit';

                        if (!empty($date->getDateStart())) {
                            $dateStart = substr($date->getDateStart(), 0, 10);
                            $start = trim(date("d-m-Y", strtotime($dateStart)));
                        }
                        if (!empty($date->getDateEnd())) {
                            $dateEnd = substr($date->getDateEnd(), 0, 10);
                            $end = trim(date("d-m-Y", strtotime($dateEnd)));
                        }
                        if (substr($date->getDateStart(), 0, 10) == substr($date->getDateEnd(), 0, 10)){
                            $dateStr = $start;
                        } else {
                            $dateStr = 'From ' . $start . ' To ' . $end;
                        }
                        $additionalOptions[] = array(
                            'label' => 'Date',
                            'value' => $dateStr
                        );
                    }
                    if (!empty($sessionId)) {
                        $session = $this->session->create()->load($sessionId);
                        $startTime = '';
                        if (!empty($session->getStartTime())) {
                            $startTime = 'Start: ' . $session->getStartTime() . ' ';
                        }
                        $endTime = '';
                        if (!empty($session->getEndTime())) {
                            $endTime = 'End: ' . $session->getEndTime();
                        }
                        $time = $startTime . $endTime;
                        $additionalOptions[] = array(
                            'label' => 'Time',
                            'value' => $time
                        );
                    }

                    if (!empty($data['ticket_info'])) {
                        $additionalOptions[] = array(
                            'label' => 'info',
                            'value' => $data['ticket_info']
                        );
                    }
                    $check = class_exists('Magento\Framework\Serialize\Serializer\Json');
                    if ($check) {
                        $item->addOption(array(
                            'code' => 'additional_options',
                            'value' => json_encode($additionalOptions)
                        ));
                    } else {
                        $item->addOption(array(
                            'code' => 'additional_options',
                            'value' => serialize($additionalOptions)
                        ));
                    }
                }
            }
        } catch (\Exception $exception) {
            \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->critical($exception);
        }
    }

    /**
     * getTitle of option
     * @param $productId
     * @param $option
     * @return mixed
     */
    public function getTitleOption($id)
    {
        return $this->option->create()->load($id)->getOptionTitle();
    }

    /**
     * @param $id
     * @return string
     */
    public function getValueOption($id)
    {
        return $this->optionType->create()->load($id)->getTitle();
    }
}
