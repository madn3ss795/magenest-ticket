<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\EventFactory;

/**
 * Class PlaceOrder
 * @package Magenest\Ticket\Observer
 */
class PlaceOrder implements ObserverInterface
{
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Ticket\Model\EventoptionDateFactory
     */
    protected $optionType;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $_session;

    /**
     * @var \Magenest\Ticket\Helper\Ticket
     */
    protected $_ticketHelper;

    /**
     * @var TicketFactory
     */
    protected $ticketFactory;

    /**
     * @var EventFactory
     */
    protected $eventFactory;

    /**
     * PlaceOrder constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Ticket\Model\EventoptionTypeFactory $eventoptionTypeFactory
     * @param TicketFactory $ticketFactory
     * @param EventFactory $eventFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Cart $cart,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Ticket\Model\EventoptionTypeFactory $eventoptionTypeFactory,
        \Magenest\Ticket\Model\EventSessionFactory $eventSessionFactory,
        \Magenest\Ticket\Helper\Ticket $ticketHelper,
        TicketFactory $ticketFactory,
        EventFactory $eventFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_cart = $cart;
        $this->optionType = $eventoptionTypeFactory;
        $this->_logger = $logger;
        $this->_ticketHelper = $ticketHelper;
        $this->_session = $eventSessionFactory;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->ticketFactory = $ticketFactory;
        $this->eventFactory = $eventFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $orderItem = $observer->getEvent()->getOrder();
            foreach ($orderItem->getAllItems() as $item) {
                $qty = $item->getQtyOrdered();
                $productType = $item->getProductType();
                $productOption = $item->getProductOptions();
                if ($productType == 'ticket') {
                    $configQty = $this->_scopeConfig->getValue(self::XML_PATH_QTY,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if ($configQty == 1) {
                        foreach ($productOption as $option) {
                            $options = $option['additional_options'];
                            if (isset($options) && !empty($options)) {
                                $this->_ticketHelper->saveQtyOption($options, $qty);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->critical($exception);
        }
    }
}
