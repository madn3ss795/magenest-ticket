<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ChangeTaxAmount
 * @package Magenest\Ticket\Observer
 */
class ChangeTaxAmount implements ObserverInterface
{
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * PlaceOrder constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Cart $cart,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_cart = $cart;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        foreach ($quote->getAllItems() as $item) {
            $productType = $item->getProductType();
            if ($productType == 'ticket') {
                $product = $item->getProduct();
                $product->setTaxClassId(0);
            }
        }
    }
}
