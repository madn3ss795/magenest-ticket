<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\EventoptionFactory;
use Magenest\Ticket\Model\EventoptionTypeFactory;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Model\EventLocationFactory;
use Magenest\Ticket\Model\EventDateFactory;
use Magenest\Ticket\Model\EventSessionFactory;
use Magento\Framework\Exception\StateException;

/**
 * Class EventTicketObserver
 *
 * @method Observer getProduct()
 */
class SaveBeforeObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Magenest\Ticket\Model\EventoptionFactory
     */
    protected $_eventoptionFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var EventoptionTypeFactory
     */
    protected $eventTypeFactory;

    /**
     * @var EventLocationFactory
     */
    protected $location;

    /**
     * @var EventDateFactory
     */
    protected $date;

    /**
     * @var EventSessionFactory
     */
    protected $session;

    /**
     * EventTicketObserver constructor.
     * @param RequestInterface $request
     * @param EventFactory $eventFactory
     * @param EventoptionFactory $eventoptionFactory
     * @param EventoptionTypeFactory $eventoptionTypeFactory
     * @param Filesystem $filesystem
     * @param UploaderFactory $fileUploaderFactory
     * @param StoreManagerInterface $storeManagerInterface
     * @param EventLocationFactory $eventLocationFactory
     * @param EventDateFactory $eventDateFactory
     * @param EventSessionFactory $eventSessionFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestInterface $request,
        EventFactory $eventFactory,
        EventoptionFactory $eventoptionFactory,
        EventoptionTypeFactory $eventoptionTypeFactory,
        Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory,
        StoreManagerInterface $storeManagerInterface,
        EventLocationFactory $eventLocationFactory,
        EventDateFactory $eventDateFactory,
        EventSessionFactory $eventSessionFactory,
        LoggerInterface $logger
    ) {
        $this->eventTypeFactory = $eventoptionTypeFactory;
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_eventFactory = $eventFactory;
        $this->_eventoptionFactory = $eventoptionFactory;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->location = $eventLocationFactory;
        $this->date = $eventDateFactory;
        $this->session = $eventSessionFactory;
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     * @throws StateException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productTypeId = $product->getTypeId();
        $params = $this->_request->getParams();
        if (!empty($params['event']) && $productTypeId == Event::PRODUCT_TYPE) {
            if (isset($params['event']['event_schedule']) && !empty($params['event']['event_schedule'])) {
                $schedule = $params['event']['event_schedule'];
                foreach ($schedule as $schedules) {
                    if (isset($schedules['row_day']) && !empty($schedules['row_day'])) {
                        $dateData = $schedules['row_day'];
                        foreach ($dateData as $dateInfo) {
                            $dataStart = @$dateInfo['time_date_start'];
                            $dataEnd = @$dateInfo['time_date_end'];
                            if (isset($dataEnd) && !empty($dataEnd)) {
                                $date1 = strtotime($dataStart);
                                $date2 = strtotime($dataEnd);

                                if ($date1 > $date2) {
                                    throw new StateException(__('On Schedule section, Date End can\'t be smaller than Date Start (Date End have empty)'));
                                }
                            }
                        }
                    }
                }
            }
        }

        return;
    }
}
