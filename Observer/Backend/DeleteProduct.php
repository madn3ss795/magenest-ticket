<?php
/**
 * Created by PhpStorm.
 * User: namnt
 * Date: 7/25/18
 * Time: 5:53 PM
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Class DeleteProduct
 * @package Magenest\Ticket\Observer\Backend
 */
class DeleteProduct implements ObserverInterface
{

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * DeleteProduct constructor.
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     */
    public function __construct(
        \Magenest\Ticket\Model\EventFactory $eventFactory
    ) {
        $this->_eventFactory = $eventFactory;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $productId = $observer->getProduct()->getEntityId();
        $event = $this->_eventFactory->create()->loadByProductId($productId);
        $event->delete();

        return;
    }
}
