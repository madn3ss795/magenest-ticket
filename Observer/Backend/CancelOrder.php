<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\EventoptionTypeFactory;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Helper\Event as HelperEvent;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Model\Configuration;
use Magenest\Ticket\Model\EventSessionFactory;

/**
 * Class GenerateTicket
 * @package Magenest\Ticket\Observer
 */
class CancelOrder implements ObserverInterface
{
    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var HelperEvent
     */
    protected $_helperEvent;

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var EventSessionFactory
     */
    protected $_session;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * CancelOrder constructor.
     * @param TicketFactory $ticketFactory
     * @param EventoptionTypeFactory $optionTypeFactory
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param EventFactory $eventFactory
     * @param HelperEvent $helperEvent
     * @param Configuration $configuration
     */
    public function __construct(
        TicketFactory $ticketFactory,
        EventoptionTypeFactory $optionTypeFactory,
        \Psr\Log\LoggerInterface $loggerInterface,
        EventFactory $eventFactory,
        EventSessionFactory $eventSessionFactory,
        HelperEvent $helperEvent,
        Configuration $configuration
    ) {
        $this->_session = $eventSessionFactory;
        $this->optionType = $optionTypeFactory;
        $this->logger = $loggerInterface;
        $this->_ticketFactory = $ticketFactory;
        $this->_eventFactory = $eventFactory;
        $this->_helperEvent = $helperEvent;
        $this->config = $configuration;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var OrderItem $orderItem */
        $orderItem = $observer->getEvent()->getItem();
        /** @var \Magento\Catalog\Model\Product $product */
        $productType = $orderItem->getProductType();
        $event = $this->_eventFactory->create()->loadByProductId($orderItem->getProductId());
        if ($event->getId() && $productType == Event::PRODUCT_TYPE) {
            $qty = $orderItem->getQtyOrdered();
            $buyInfo = $orderItem->getBuyRequest();
            $options = $buyInfo->getAdditionalOptions();
            if (isset($options) && !empty($options)) {
                if (isset($options['dropdown']) && !empty($options['dropdown'])) {
                    foreach ($options['dropdown'] as $dropdown => $value) {
                        if ($value != "") {
                            $this->saveOption($value, $qty);
                        }
                    }
                }
                if (isset($options['radio']) && !empty($options['radio'])) {
                    foreach ($options['radio'] as $radio => $value) {
                        $this->saveOption($value, $qty);
                    }
                }
                if (isset($options['checkbox']) && !empty($options['checkbox'])) {
                    foreach ($options['checkbox'] as $checkbox) {
                        foreach ($checkbox as $checkboxSelected => $value) {
                            $this->saveOption($value, $qty);
                        }
                    }
                }
                if (@$options['single']['ticket_session'])
                {
                    $session = $this->_session->create()->load($options['single']['ticket_session']);
                    $qtyAvailable = $session->getQtyAvailable() + $qty;
                    $qtyPurchase = $session->getQtyPurchase() - $qty;
                    $session->setQtyAvailable($qtyAvailable);
                    $session->setQtyPurchase($qtyPurchase);
                    $session->save();
                }
                else if (@$options['ticket_session']) {
                    $session = $this->_session->create()->load($options['ticket_session']);
                    $qtyAvailable = $session->getQtyAvailable() + $qty;
                    $qtyPurchase = $session->getQtyPurchase() - $qty;
                    $session->setQtyAvailable($qtyAvailable);
                    $session->setQtyPurchase($qtyPurchase);
                    $session->save();
                }
            }
        }

        return;
    }

    /**
     * @param $title
     * @param $productId
     * @param $qty
     */
    public function saveOption($data, $qty)
    {
        $data = explode("_", $data);
        $id = $data[1];
        $eventoption = $this->optionType->create()->load($id);
        $availableQty = $eventoption->getAvailableQty() + $qty;
        $purchasedQty = $eventoption->getPurcharsedQty() - $qty;
        $revenue = ($eventoption->getPrice()) * ($purchasedQty);

        $eventoption->setAvailableQty($availableQty);
        $eventoption->setPurcharsedQty($purchasedQty);
        $eventoption->setRevenue($revenue);
        $eventoption->save();
    }
}
