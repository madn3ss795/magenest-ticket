<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\EventoptionFactory;
use Magenest\Ticket\Model\EventoptionTypeFactory;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\EventSessionFactory;
use Magenest\Ticket\Helper\Event as HelperEvent;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Helper\Information;

/**
 * Class GenerateTicket
 * @package Magenest\Ticket\Observer
 */
class GenerateTicket implements ObserverInterface
{

    /**
     * email config
     */
    const XML_PATH_EMAIL = 'event_ticket/email_config/email';

    /**
     * qty config
     */
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';
    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var EventoptionFactory
     */
    protected $_eventoptionFactory;

    /**
     * @var HelperEvent
     */
    protected $_helperEvent;

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var EventSessionFactory
     */
    protected $_session;

    /**
     * @var \Magenest\Ticket\Helper\Ticket
     */
    protected $_ticketHelper;

    /**
     * GenerateTicket constructor.
     * @param TicketFactory $ticketFactory
     * @param EventoptionFactory $eventoptionFactory
     * @param EventoptionTypeFactory $optionTypeFactory
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param EventFactory $eventFactory
     * @param HelperEvent $helperEvent
     * @param Information $information
     */
    public function __construct(
        TicketFactory $ticketFactory,
        EventoptionFactory $eventoptionFactory,
        EventoptionTypeFactory $optionTypeFactory,
        EventSessionFactory $eventSessionFactory,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magenest\Ticket\Helper\Ticket $ticketHelper,
        EventFactory $eventFactory,
        HelperEvent $helperEvent,
        Information $information
    ) {
        $this->_session = $eventSessionFactory;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->optionType = $optionTypeFactory;
        $this->logger = $loggerInterface;
        $this->_ticketHelper = $ticketHelper;
        $this->_ticketFactory = $ticketFactory;
        $this->_eventoptionFactory = $eventoptionFactory;
        $this->_eventFactory = $eventFactory;
        $this->_helperEvent = $helperEvent;
        $this->information = $information;
        $this->messageManager = $messageManager;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var OrderItem $orderItem */
            $orderItem = $observer->getEvent()->getItem();
            /** @var \Magento\Catalog\Model\Product $product */
            $productType = $orderItem->getProductType();
            $buyInfo = $orderItem->getBuyRequest();
            $options = $buyInfo->getAdditionalOptions();
            $event = $this->_eventFactory->create()->loadByProductId($orderItem->getProductId());
            if ($event->getId()
                && $productType == Event::PRODUCT_TYPE
                && $orderItem->getStatusId() == OrderItem::STATUS_INVOICED) {
                if (!$this->_helperEvent->getTicket($orderItem->getId())) {
                    /** @var \Magento\Sales\Model\Order $order */
                    $order = $orderItem->getOrder();
                    $qty = $orderItem->getQtyOrdered();
                    $email = $order->getCustomerEmail();
                    $firstname = $order->getCustomerFirstname();
                    $lastname = $order->getCustomerLastname();
                    $customerId = $order->getCustomerId();
                    $customerName = $firstname . " " . $lastname;

                    if (!$customerId) {
                        $customerName = 'Guest';
                    }

                    $optionsChosen = [];
                    if (isset($options['dropdown']) && !empty($options['dropdown'])) {
                        foreach ($options['dropdown'] as $dropdown => $value) {
                            if ($value) {
                                $optionsChosen[] = explode("_", $value)[1];
                            }
                        }
                    }

                    if (isset($options['radio']) && !empty($options['radio'])) {
                        foreach ($options['radio'] as $radio => $value) {
                            $optionsChosen[] = explode("_", $value)[1];
                        }
                    }
                    if (isset($options['checkbox']) && !empty($options['checkbox'])) {
                        foreach ($options['checkbox'] as $checkbox) {
                            foreach ($checkbox as $key => $value) {
                                $optionsChosen[] = explode("_", $value)[1];
                            }
                        }
                    }

                    foreach ($optionsChosen as $key => $value) {
                        $optionsChosen[$key] = $this->_ticketHelper->getTitleOptionType($value);
                    }
                    $optionInfo = implode(",", $optionsChosen);

                    $configEmail = $this->_scopeConfig->getValue(self::XML_PATH_EMAIL,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if ($configEmail == 'send_multi_email') {
                        $putQty = 1;
                        $number = $qty;
                    } else {
                        $putQty = $qty;
                        $number = 1;
                    }
                    $arrayInformation = $this->information->getAll($options);

                    $ticketData = [
                        'title' => $orderItem->getName(),
                        'event_id' => $event->getId(),
                        'product_id' => $orderItem->getProductId(),
                        'customer_name' => $customerName,
                        'customer_email' => $email,
                        'customer_id' => $customerId,
                        'order_item_id' => $orderItem->getId(),
                        'order_id' => $order->getId(),
                        'order_increment_id' => $order->getIncrementId(),
                        'note' => $optionInfo,
                        'information' => serialize($arrayInformation),
                        'qty' => $putQty,
                        'status' => 1,
                    ];
                    $isRegister = false;

                    if ($buyInfo->getTicketRegister()) {
                        $tickeRegisters = json_decode($buyInfo->getTicketRegister(), true);
                        foreach ($tickeRegisters as $register) {
                            /** @var array $ticketData */
                            $ticketData['code'] = $this->_helperEvent->generateCode();
                            $ticketData['customer_name'] = @$register['fn'] . " " . @$register['ln'];
                            $ticketData['customer_email'] = @$register['e'];
                            $model = $this->_ticketFactory->create();
                            $model->setData($ticketData)->save();
                        }
                        $isRegister = true;
                    } else {
                        for ($i = 0; $i < $number; $i++) {
                            /** @var array $ticketData */
                            $ticketData['code'] = $this->_helperEvent->generateCode();
                            $model = $this->_ticketFactory->create();
                            $model->setData($ticketData)->save();
                        }
                    }

                    $modelTicket = $this->_ticketFactory->create()->getCollection()
                        ->addFieldToFilter('event_id', $event->getId())
                        ->addFieldToFilter('product_id', $orderItem->getProductId())
                        ->addFieldToFilter('order_increment_id', $order->getIncrementId());
                    foreach ($modelTicket as $ticketMail) {
                        $this->_ticketFactory->create()->sendMail($ticketMail->getTicketId(), $isRegister);
                    }

                    $configQty = $this->_scopeConfig->getValue(self::XML_PATH_QTY,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if ($configQty == 2) {
                        if (isset($options) && !empty($options)) {
                            $this->_ticketHelper->saveQtyOption($options, $qty);
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        return;
    }
}
