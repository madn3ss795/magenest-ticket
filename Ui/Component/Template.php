<?php

namespace Magenest\Ticket\Ui\Component;

use Magenest\Ticket\Model\TemplateFactory;
use Magento\Framework\Option\ArrayInterface;

/**
 * Class Country
 * @package Magenest\Groupon\Ui\Component
 */
class Template implements ArrayInterface
{
    /**
     * @var
     */
    protected $template;

    /**
     * Template constructor.
     * @param TemplateFactory $template
     */
    public function __construct(
        TemplateFactory $template
    ) {
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $model = $this->template->create()->getCollection();
        $result[] = [
            'label' => __("---Choose Template---"),
            'value' => null
        ];
        foreach ($model as $temp) {
            $array = [
                'label' => $temp->getTitle(),
                'value' => $temp->getTemplateId()
            ];
            array_push($result, $array);
        }

        return $result;
    }
}
