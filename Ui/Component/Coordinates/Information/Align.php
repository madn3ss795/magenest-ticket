<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Ui\Component\Coordinates\Information;

/**
 * Class Align
 * @package Magenest\Ticket\Ui\Component\Coordinates\Information
 */
class Align implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('--------'), 'value' => null],
            ['label' => __('Left'), 'value' => 'left'],
            ['label' => __('Center'), 'value' => 'center'],
            ['label' => __('Right'), 'value' => 'right'],
        ];
    }
}
