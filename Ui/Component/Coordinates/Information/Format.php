<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Ui\Component\Coordinates\Information;

/**
 * Class Format
 * @package Magenest\Ticket\Ui\Component\Coordinates\Information
 */
class Format implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('--------'), 'value' => null],
            ['label' => __('Regular'), 'value' => 'regular'],
            ['label' => __('Bold'), 'value' => 'bold'],
            ['label' => __('Italic'), 'value' => 'italic'],
            ['label' => __('Bold & Italic'), 'value' => 'bold_italic'],

        ];
    }
}
