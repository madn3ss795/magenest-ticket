<?php
/**
 * Created by PhpStorm.
 * User: namnt
 * Date: 8/9/18
 * Time: 6:36 PM
 */

namespace Magenest\Ticket\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Ticket
 * @package Magenest\Ticket\Helper
 */
class Ticket extends AbstractHelper
{

    /**
     * @var \Magenest\Ticket\Model\EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $_session;

    /**
     * @var \Magenest\Ticket\Model\EventoptionFactory
     */
    protected $_eventoptionFactory;

    /**
     * @var \Magenest\Ticket\Helper\Information
     */
    protected $information;

    /**
     * Event constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param TicketFactory $ticketFactory
     */
    public function __construct(
        \Magenest\Ticket\Model\EventoptionTypeFactory $optionType,
        \Magenest\Ticket\Model\EventSessionFactory $eventSessionFactory,
        \Magenest\Ticket\Model\EventoptionFactory $eventoptionFactory,
        \Magenest\Ticket\Helper\Information $information,
        Context $context
    ) {
        $this->optionType = $optionType;
        $this->information = $information;
        $this->_eventoptionFactory = $eventoptionFactory;
        $this->_session = $eventSessionFactory;
        parent::__construct($context);
    }

    /**
     * @param $data
     * @param $qty
     */
    public function saveOption($data, $qty)
    {
        $data = explode("_", $data);
        $id = $data[1];
        $eventoption = $this->optionType->create()->load($id);
        $availableQty = $eventoption->getAvailableQty() - $qty;
        $purchasedQty = $eventoption->getPurcharsedQty() + $qty;
        $revenue = ($eventoption->getPrice()) * ($purchasedQty);
        $eventoption->setAvailableQty($availableQty);
        $eventoption->setPurcharsedQty($purchasedQty);
        $eventoption->setRevenue($revenue);
        $eventoption->save();
    }

    public function saveQtyOption($options, $qty)
    {
        if (isset($options['dropdown']) && !empty($options['dropdown'])) {
            foreach ($options['dropdown'] as $dropdown => $value) {
                if ($value != "") {
                    $this->saveOption($value, $qty);
                }
            }
        }
        if (isset($options['radio']) && !empty($options['radio'])) {
            foreach ($options['radio'] as $radio => $value) {
                $this->saveOption($value, $qty);
            }
        }
        if (isset($options['checkbox']) && !empty($options['checkbox'])) {
            foreach ($options['checkbox'] as $checkbox) {
                foreach ($checkbox as $checkboxSelected => $value) {
                    $this->saveOption($value, $qty);
                }
            }
        }
        /**Check if single day event, pass correct array to function*/
        if (@$options['single']['ticket_session']) {
            $session = $this->_session->create()->load($options['single']['ticket_session']);
            $qtyAvailable = $session->getQtyAvailable() - $qty;
            $qtyPurchase = $session->getQtyPurchase() + $qty;
            $session->setQtyAvailable($qtyAvailable);
            $session->setQtyPurchase($qtyPurchase);
            $session->save();
        }
        else
        if (@$options['ticket_session']) {
            $session = $this->_session->create()->load($options['ticket_session']);
            $qtyAvailable = $session->getQtyAvailable() - $qty;
            $qtyPurchase = $session->getQtyPurchase() + $qty;
            $session->setQtyAvailable($qtyAvailable);
            $session->setQtyPurchase($qtyPurchase);
            $session->save();
        }
    }

    /**
     * @param $options
     * @return array
     */
    public function getInfoTicket($options)
    {
        $data = $this->information->getAll($options);
        $info = $this->information->getDataTicket($data);

        return $info;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOptionType($id)
    {
        $optionType = $this->optionType->create()->load($id);

        return $optionType->getTitle();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOption($id)
    {
        $optionType = $this->_eventoptionFactory->create()->load($id);

        return $optionType->getOptionTitle();
    }
}
