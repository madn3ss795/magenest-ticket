<?php

namespace Magenest\Ticket\Model;

/**
 * Class DataProvider
 * @package Magenest\Ticket\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * DataProvider constructor.
     * @param TemplateFactory $templateFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param array $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magenest\Ticket\Model\TemplateFactory $templateFactory,
        $name,
        $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $templateFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get('Magento\Framework\App\RequestInterface');
        $id = $request->getParam('template_id');
        if ($id) {
            $model = $objectManager->create('Magenest\Ticket\Model\Template')->load($id);
            $data[$id] = $model->getData();
            $data[$id]['pdf_coordinates'] = unserialize($data[$id]['pdf_coordinates']);
            if (@$data[$id]['pdf_background']) {
                $data[$id]['pdf_background'] = unserialize($data[$id]['pdf_background']);
            }
        }

        return $data;
    }
}
