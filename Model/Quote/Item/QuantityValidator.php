<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Model\Quote\Item;

use Magenest\Ticket\Model\EventoptionFactory;
use Magenest\Ticket\Model\EventSessionFactory;
use Magenest\Ticket\Model\EventoptionTypeFactory;
use Magento\Framework\Event\Observer;
use Magento\CatalogInventory\Helper\Data;
use Magento\Setup\Exception;
use Psr\Log\LoggerInterface;

/**
 * Class QuantityValidator
 * @package Magenest\Ticket\Model\Quote\Item
 */
class QuantityValidator
{
    /**
     * @var EventoptionFactory
     */
    protected $_eventoptionFactory;

    /**
     * @var EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var EventSessionFactory
     */
    protected $_session;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * QuantityValidator constructor.
     * @param EventoptionFactory $eventoptionFactory
     * @param EventoptionTypeFactory $optionTypeFactory
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        EventoptionFactory $eventoptionFactory,
        EventSessionFactory $eventSessionFactory,
        EventoptionTypeFactory $optionTypeFactory,
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        LoggerInterface $loggerInterface
    ) {
        $this->_session = $eventSessionFactory;
        $this->messageManager = $managerInterface;
        $this->optionType = $optionTypeFactory;
        $this->_eventoptionFactory = $eventoptionFactory;
        $this->_logger = $loggerInterface;
    }

    /**
     * Check product option event data when quote item quantity declaring
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function validate(Observer $observer)
    {
        try {
            /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
            $quoteItem = $observer->getEvent()->getItem();
            $buyInfo = $quoteItem->getBuyRequest();
            $options = $buyInfo->getAdditionalOptions();
            if (!$quoteItem ||
                !$quoteItem->getProductId() ||
                !$quoteItem->getQuote() ||
                $quoteItem->getQuote()->getIsSuperMode()
            ) {
                return;
            }
            if ($options) {
                if (@$options['dropdown'] && count($options['dropdown']) > 0) {
                    foreach ($options['dropdown'] as $option) {
                        $optionDropdown = explode("_", $option);
                        if ($optionDropdown[0] == "" || $optionDropdown[0] == null) {
                            continue;
                        }
                        $this->getErrorMessage($optionDropdown[1], $quoteItem);
                    }
                }
                if (@$options['radio'] && count($options['radio']) > 0) {
                    foreach ($options['radio'] as $option) {
                        $optionRadio = explode("_", $option);
                        $this->getErrorMessage($optionRadio[1], $quoteItem);
                    }
                }
                if (@$options['checkbox'] && count($options['checkbox']) > 0) {
                    foreach ($options['checkbox'] as $optionCheckbox) {
                        foreach ($optionCheckbox as $optionTypeId => $value) {
                            $this->getErrorMessage($optionTypeId, $quoteItem);
                        }
                    }
                }

                /**Check if single day event, pass correct array to validator*/
                if (@$options["single"]["ticket_session"]) {
                    $this->getErrorMessageSession($options["single"]["ticket_session"], $quoteItem);
                }
                else{
                    $this->getErrorMessageSession($options["ticket_session"], $quoteItem);
                }
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());

            return;
        }
    }

    /**
     * get Error if not enough qty
     * @param $type
     * @param $quoteItem
     */
    public function getErrorMessageSession($sessionId, $quoteItem)
    {
        /** @var \Magenest\Ticket\Model\EventoptionType $modelType */
        $model = $this->_session->create()->load($sessionId);

        $availableQty = $model->getQtyAvailable();
        $limitQty = $model->getLimitQty();
        if ($availableQty && ($quoteItem->getQty()) > $availableQty && $limitQty == 1) {
            $quoteItem->addErrorInfo(
                'erro_info',
                Data::ERROR_QTY,
                __('We don\'t have as many tickets for this session as you requested. You can buy maximum '
                    . $availableQty . ' tickets for this session.')
            );
            return;
        }
    }

    /**
     * get Error if not enough qty
     * @param $type
     * @param $quoteItem
     */
    public function getErrorMessage($typeId, $quoteItem)
    {
        /** @var \Magenest\Ticket\Model\EventoptionType $modelType */
        $model = $this->optionType->create()->load($typeId);
        $availableQty = $model->getAvailableQty();
        $title = $model->getTitle();
        if ($availableQty && ($quoteItem->getQty()) > $availableQty) {
            $quoteItem->addErrorInfo(
                'erro_info',
                Data::ERROR_QTY,
                __('We don\'t have as many option "' . $title
                    . '" as you requested. You can buy maximum ' . $availableQty
                    . ' ticket(s) with this option.')
            );

            return;
        }
    }
}
