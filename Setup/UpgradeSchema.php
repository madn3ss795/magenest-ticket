<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Setup;

use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * @package Magenest\Ticket\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrade database when run bin/magento setup:upgrade from command line
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_ticket'),
                'product_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Product Id'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_ticket'),
                'information',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Information'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_ticket'),
                'qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'comment' => 'Qty'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'email_config',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Email Config'
                ]
            );

            $this->createTicketLocationTable($installer);

            $this->createTicketDateTable($installer);

            $this->createTicketSessionTable($installer);
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'enable_date_time',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Enabled Date Time'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event_session'),
                'max_qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Max Qty'
                ]
            );
        }

        if (version_compare($context->getVersion(), '101.1.0') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'allow_register',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Allow Customer Register Ticket'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'is_register_required',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Set Register Required'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'template_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Template Id'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'use_custom_template',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => 2,
                    'nullable' => true,
                    'comment' => 'Use Custom Template'
                ]
            );
            $this->createTicketTemplateTable($installer);
        }

        if (version_compare($context->getVersion(), '101.2.0') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'term',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => null,
                    'nullable' => true,
                    'comment' => 'Terms and Conditions'
                ]
            );
        }

        if (version_compare($context->getVersion(), '101.2.1') < 0) {
            $this->addQtyForSession($installer);
        }

        if (version_compare($context->getVersion(), '101.2.2') < 0) {
            $this->addApplyLocationsForAllOptions($installer);
        }

        $installer->endSetup();
    }

    private function addApplyLocationsForAllOptions($installer)
    {
        $table = $installer->getTable('magenest_ticket_event');
        $connection = $installer->getConnection();
        $connection->addColumn(
            $table,
            'apply_all_schedule',
            [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Is apply for all options'
            ]);
        $table = $installer->getTable('magenest_ticket_eventoption_type');
        $connection = $installer->getConnection();
        $connection->addColumn(
            $table,
            'start_date',
            [
                'type' => Table::TYPE_DATETIME,
                'comment' => 'Start Date'
            ]);
        $connection->addColumn(
            $table,
            'end_date',
            [
                'type' => Table::TYPE_DATETIME,
                'comment' => 'End Date'
            ]);
        $connection->addColumn(
            $table,
            'location_title',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Location Title'
            ]);
        $connection->addColumn(
            $table,
            'location_address',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Location Address'
            ]);
    }

    private function addQtyForSession($installer)
    {
        $table = $installer->getTable('magenest_ticket_event_session');
        $connection = $installer->getConnection();
        $connection->addColumn(
            $table,
            'qty',
            [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'qty'
            ]);
        $connection->addColumn(
            $table,
            'limit_qty',
            [
                'type' => Table::TYPE_SMALLINT,
                'comment' => 'Limit Qty'
            ]);
        $connection->addColumn(
            $table,
            'qty_available',
            [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Qty Available'
            ]);
        $connection->addColumn(
            $table,
            'qty_purchase',
            [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Qty Purchase'
            ]);
    }

    /**
     * Create the table name magenest_ticket_event_location
     *
     * @param SetupInterface $installer
     * @return void
     * @throws \Zend_Db_Exception
     */
    private function createTicketLocationTable($installer)
    {
        $tableName = 'magenest_ticket_event_location';
        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'location_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Location ID'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Product Id'
        )->addColumn(
            'location_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Location Title'
        )->addColumn(
            'location_detail',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Location Detail'
        )->addColumn(
            'location_is_enabled',
            Table::TYPE_SMALLINT,
            2,
            ['nullable' => true],
            'Location Is Enabled'
        )->setComment('Event Location Table');

        $installer->getConnection()->createTable($table);
    }

    /**
     * Create the table name magenest_ticket_event_date
     *
     * @param SetupInterface $installer
     * @return void
     * @throws \Zend_Db_Exception
     */
    private function createTicketDateTable($installer)
    {
        $tableName = 'magenest_ticket_event_date';
        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'date_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Date ID'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Product Id'
        )->addColumn(
            'event_location_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Event Location Id'
        )->addColumn(
            'date_start',
            Table::TYPE_DATETIME,
            null,
            [],
            'Date Start'
        )->addColumn(
            'date_end',
            Table::TYPE_DATETIME,
            null,
            [],
            'Date End'
        )->addColumn(
            'date_is_enabled',
            Table::TYPE_SMALLINT,
            2,
            ['nullable' => true],
            'Date Is Enabled'
        )->setComment('Event Date Table');

        $installer->getConnection()->createTable($table);
    }

    /**
     * Create the table name magenest_ticket_event_session
     *
     * @param SetupInterface $installer
     * @return void
     * @throws \Zend_Db_Exception
     */
    private function createTicketSessionTable($installer)
    {
        $tableName = 'magenest_ticket_event_session';
        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'session_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Session ID'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Product Id'
        )->addColumn(
            'event_date_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Event Date Id'
        )->addColumn(
            'start_time',
            Table::TYPE_TEXT,
            null,
            [],
            'Start Time'
        )->addColumn(
            'end_time',
            Table::TYPE_TEXT,
            null,
            [],
            'End Time'
        )->addColumn(
            'session_is_enabled',
            Table::TYPE_SMALLINT,
            2,
            ['nullable' => true],
            'Session Is Enabled'
        )->setComment('Event Session Table');

        $installer->getConnection()->createTable($table);
    }

    /**
     * table template
     * @param $installer
     */
    private function createTicketTemplateTable($installer)
    {
        $tableName = 'magenest_ticket_template';

        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'template_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Location ID'
        )->addColumn(
            'enable',
            Table::TYPE_INTEGER,
            2,
            ['nullable' => true, 'default' => 0],
            'Enable'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Title'
        )->addColumn(
            'pdf_coordinates',
            Table::TYPE_TEXT,
            '64K',
            [],
            'PDF Ticket Coordinates'
        )->addColumn(
            'pdf_page_width',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Width'
        )->addColumn(
            'pdf_page_height',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Height'
        )->addColumn(
            'pdf_background',
            Table::TYPE_TEXT,
            null,
            [],
            'PDF Ticket Background'
        )->setComment('PDF Template');

        $installer->getConnection()->createTable($table);
    }
}
