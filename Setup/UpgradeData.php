<?php
/**
 * Created by PhpStorm.
 * User: namnt
 * Date: 7/11/18
 * Time: 2:41 PM
 */

namespace Magenest\Ticket\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Model\Product;

/**
 * Class UpgradeData
 * @package Magenest\Ticket\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '101.2.3', '<')) {
            $this->createDefaultTemplate();
        }

        if (version_compare($context->getVersion(), '101.2.4', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $applyTo = explode(
                ',',
                $eavSetup->getAttribute(Product::ENTITY, 'tax_class_id', 'apply_to')
            );
            if (!in_array('ticket', $applyTo)) {
                $applyTo[] = 'ticket';
                $eavSetup->updateAttribute(Product::ENTITY, 'tax_class_id', 'apply_to', implode(',', $applyTo));
            }
        }
    }

    public function createDefaultTemplate()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $template = $objectManager->create('Magenest\Ticket\Model\Template');
        $data = [
            'enable' => 1,
            'title' => 'Default Template',
            'pdf_coordinates' => 'a:14:{i:0;a:10:{s:5:"title";s:10:"event_name";s:1:'
                . '"x";s:3:"300";s:1:"y";s:3:"310";s:4:"size";s:2:"10";s:9:"record_id"'
                . ';s:1:"0";s:4:"info";s:10:"event_name";s:5:"align";s:6:"center";s:6:"'
                . 'format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initialize";s:4:"'
                . 'true";}i:1;a:10:{s:9:"record_id";s:1:"1";s:4:"info";s:11:"event_photo";'
                . 's:5:"title";s:0:"";s:1:"x";s:3:"100";s:1:"y";s:3:"330";s:4:"size";s:2:"70'
                . '";s:5:"align";s:6:"center";s:6:"format";s:7:"regular";s:5:"color";s:5:"'
                . 'black";s:10:"initialize";s:4:"true";}i:2;a:10:{s:9:"record_id";s:1:"'
                . '2";s:4:"info";s:15:"location_detail";s:5:"title";s:15:"location_detail'
                . '";s:1:"x";s:3:"300";s:1:"y";s:3:"130";s:4:"size";s:2:"10";s:5:"align";'
                . 's:6:"center";s:6:"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"'
                . 'initialize";s:4:"true";}i:3;a:10:{s:9:"record_id";s:1:"3";s:4:"info";s:'
                . '7:"qr_code";s:5:"title";s:7:"qr_code";s:1:"x";s:3:"250";s:1:"y";s:3:"280"'
                . ';s:4:"size";s:3:"100";s:5:"align";s:6:"center";s:6:"format";s:7:"regular";'
                . 's:5:"color";s:5:"black";s:10:"initialize";s:4:"true";}i:4;a:10:{s:9:"'
                . 'record_id";s:1:"4";s:4:"info";s:8:"bar_code";s:5:"title";s:8:"bar_code";'
                . 's:1:"x";s:3:"250";s:1:"y";s:3:"100";s:4:"size";s:3:"100";s:5:"align";s:6:"'
                . 'center";s:6:"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initialize"'
                . ';s:4:"true";}i:5;a:10:{s:9:"record_id";s:1:"5";s:4:"info";s:4:"code";s:5:"'
                . 'title";s:4:"code";s:1:"x";s:3:"200";s:1:"y";s:2:"50";s:4:"size";s:2:"10";'
                . 's:5:"align";s:6:"center";s:6:"format";s:7:"regular";s:5:"color";s:5:"black'
                . '";s:10:"initialize";s:4:"true";}i:6;a:10:{s:9:"record_id";s:1:"6";s:4:"info'
                . '";s:4:"type";s:5:"title";s:11:"ticket_type";s:1:"x";s:3:"300";s:1:"y";s:3:"'
                . '290";s:4:"size";s:2:"10";s:5:"align";s:6:"center";s:6:"format";s:7:"regular'
                . '";s:5:"color";s:5:"black";s:10:"initialize";s:4:"true";}i:7;a:10:{s:'
                . '9:"record_id";s:1:"7";s:4:"info";s:4:"date";s:5:"title";s:4:"date";s:1'
                . ':"x";s:3:"300";s:1:"y";s:3:"270";s:4:"size";s:2:"10";s:5:"align";s:6:"c'
                . 'enter";s:6:"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initial'
                . 'ize";s:4:"true";}i:8;a:10:{s:9:"record_id";s:1:"8";s:4:"info";s:10:"start_time"'
                . ';s:5:"title";s:10:"start_time";s:1:"x";s:3:"300";s:1:"y";s:3:"250";s:4:"size";'
                . 's:2:"10";s:5:"align";s:6:"center";s:6:"format";s:7:"regular";s:5:"color'
                . '";s:5:"black";s:10:"initialize";s:4:"true";}i:9;a:10:{s:9:"record_id";'
                . 's:1:"9";s:4:"info";s:8:"end_time";s:5:"title";s:8:"end_time";s:1:"x"'
                . ';s:3:"300";s:1:"y";s:3:"230";s:4:"size";s:2:"10";s:5:"align";s:6:'
                . '"center";s:6:"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"'
                . 'initialize";s:4:"true";}i:10;a:10:{s:9:"record_id";s:2:"10";s:4:"info'
                . '";s:13:"customer_name";s:5:"title";s:13:"customer_name";s:1:"x";s:3:'
                . '"300";s:1:"y";s:3:"210";s:4:"size";s:2:"10";s:5:"align";s:6:"center"'
                . ';s:6:"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initialize"'
                . ';s:4:"true";}i:11;a:10:{s:9:"record_id";s:2:"11";s:4:"info";s:14:"'
                . 'customer_email";s:5:"title";s:14:"customer_email";s:1:"x";s:3:"300"'
                . ';s:1:"y";s:3:"190";s:4:"size";s:2:"10";s:5:"align";s:6:"center";s:6:"'
                . 'format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initialize";s:4'
                . ':"true";}i:12;a:10:{s:9:"record_id";s:2:"12";s:4:"info";s:18:'
                . '"order_increment_id";s:5:"title";s:5:"order";s:1:"x";s:3:"300";'
                . 's:1:"y";s:3:"100";s:4:"size";s:2:"10";s:5:"align";s:6:"center";s:6'
                . ':"format";s:7:"regular";s:5:"color";s:5:"black";s:10:"initialize"'
                . ';s:4:"true";}i:13;a:10:{s:9:"record_id";s:2:"13";s:4:"info";s:3:"'
                . 'qty";s:5:"title";s:8:"quantity";s:1:"x";s:3:"300";s:1:"y";s:3:"150"'
                . ';s:4:"size";s:2:"10";s:5:"align";s:6:"center";s:6:"format";s:7:"r'
                . 'egular";s:5:"color";s:5:"black";s:10:"initialize";s:4:"true";}}',
            'pdf_page_width' => 600,
            'pdf_page_height' => 400,
            'pdf_background' => null
        ];
        $template->addData($data);
        $template->save();
    }
}
