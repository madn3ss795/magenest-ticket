<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml\Ticket;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magenest\Ticket\Controller\Adminhtml\Ticket as TicketController;

/**
 * Class MassStatus
 * @package Magenest\Ticket\Controller\Adminhtml\Ticket
 */
class MassStatus extends TicketController
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $status = (int)$this->getRequest()->getParam('status');
        $totals = 0;
        try {
            foreach ($collection as $item) {
                /** @var \Magenest\Ticket\Model\Ticket $item */
                $item->setStatus($status)->save();
                $totals++;
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been updated.', $totals));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
