<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml;

use Magenest\Ticket\Model\TemplateFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Ticket\Model\ResourceModel\Template\CollectionFactory as TemplateCollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Template
 * @package Magenest\Ticket\Controller\Adminhtml
 */
abstract class Template extends Action
{
    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var TemplateCollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Template constructor.
     * @param TemplateFactory $templateFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param TemplateCollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        TemplateFactory $templateFactory,
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        TemplateCollectionFactory $collectionFactory,
        Filter $filter
    ) {
        $this->_templateFactory = $templateFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }

        return $this->_resultPage;
    }

    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Ticket::template');
        $resultPage->getConfig()->getTitle()->prepend((__('PDF Template')));

        return $this;
    }
}
