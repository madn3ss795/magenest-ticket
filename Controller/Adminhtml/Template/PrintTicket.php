<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 02/05/2018
 * Time: 19:29
 */

namespace Magenest\Ticket\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Ticket\Helper\Pdf as PdfHelper;

/**
 * Class PrintTicket
 * @package Magenest\Ticket\Controller\Adminhtml\Template
 */
class PrintTicket extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * @var \Magenest\Ticket\Model\TemplateFactory
     */
    protected $_templateFactory;

    /**
     * PrintTicket constructor.
     * @param Action\Context $context
     * @param PdfHelper $pdfHelper
     * @param \Magenest\Ticket\Model\TemplateFactory $templateFactory
     * @param PageFactory $resultPageFactory
     * @param FileFactory $fileFactory
     */
    public function __construct(
        Action\Context $context,
        PdfHelper $pdfHelper,
        \Magenest\Ticket\Model\TemplateFactory $templateFactory,
        PageFactory $resultPageFactory,
        FileFactory $fileFactory
    ) {

        $this->fileFactory = $fileFactory;
        $this->_pdfHelper = $pdfHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->_templateFactory = $templateFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $templateId = (int)$this->getRequest()->getParam('id');
        if ($templateId) {
            try {
                $ticketTemplate = $this->_templateFactory->create()->load($templateId);
                if ($ticketTemplate->getId()) {
                    return $this->fileFactory->create(
                        sprintf('template-%s.pdf', $ticketTemplate->getTitle()),
                        $this->_pdfHelper->getPrintPdfPreview($ticketTemplate->getData())->render(),
                        DirectoryList::VAR_DIR,
                        'application/pdf'
                    );
                }
            } catch (\Exception $exception) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $objectManager->get('Psr\Log\LoggerInterface')->critical($exception);
            }
        }

        return $resultPage;
    }
}
