<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml\Template;

use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magenest\Ticket\Helper\Pdf as PdfHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Save
 * @package Magenest\Ticket\Controller\Adminhtml\Template
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * Save constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        PdfHelper $pdfHelper
    ) {
        $this->fileFactory = $fileFactory;
        $this->_pdfHelper = $pdfHelper;
        parent::__construct($context);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (@$post['pdf_coordinates']) {
            foreach ($post['pdf_coordinates'] as $key => $pdf_coordinate) {
                if (@$pdf_coordinate['is_delete']) {
                    unset($post['pdf_coordinates'][$key]);
                }
                if (@$pdf_coordinate['x']) {
                    if ($pdf_coordinate['x'] > $post['pdf_page_width']) {
                        $this->messageManager->addErrorMessage(__('Coordinate X cannot exceed the Page Width.'));

                        $post['pdf_coordinates'][$key]['x'] = 0;
                    }
                }
                if (@$pdf_coordinate['y']) {
                    if ($pdf_coordinate['y'] > $post['pdf_page_height']) {
                        $this->messageManager->addNoticeMessage(__('Coordinate Y cannot exceed the Page Height.'));

                        $post['pdf_coordinates'][$key]['y'] = 0;
                    }
                }
            }
        } else {
            $post['pdf_coordinates'] = [];
        }

        $post['pdf_coordinates'] = array_values($post['pdf_coordinates']);

        if (!$post) {
            return $resultRedirect->setPath('*/*/');
        }
        try {
            $array = [
                'title' => @$post['title'],
                'enable' => @$post['enable'],
                'pdf_page_width' => @$post['pdf_page_width'],
                'pdf_page_height' => @$post['pdf_page_height'],
                'pdf_background' => isset($post['pdf_background']) ? serialize($post['pdf_background']) : serialize([]),
                'pdf_coordinates' => isset($post['pdf_coordinates']) ? serialize($post['pdf_coordinates']) : serialize([]),
            ];

            $model = $this->_objectManager->create('Magenest\Ticket\Model\Template');

            if(@$post['template_id']){
                $model->load($post['template_id']);
            }
            $model->addData($array);
            $model->save();
            $this->messageManager->addSuccessMessage(__('The template has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            return $resultRedirect->setPath('*/template/edit', ['template_id' => $model->getTemplateId()]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the rule.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('*/*/edit',
                ['template_id' => $this->getRequest()->getParam('template_id')]);
        }

        return $resultRedirect->setPath('*/template/edit', ['template_id' => $model->getTemplateId()]);
    }
}
