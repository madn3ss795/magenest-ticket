<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml\Template;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Magenest\Ticket\Controller\Adminhtml\Template
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $id = $this->getRequest()->getParam('template_id');
        if ($id) {
            $model = $this->_objectManager->create('Magenest\Ticket\Model\Template')->load($id);
            $title = "Edit Template: " . $model->getTitle();
        } else {
            $title = 'New Template';
        }
        $resultPage->getConfig()->getTitle()->prepend($title);

        return $resultPage;
    }
}
