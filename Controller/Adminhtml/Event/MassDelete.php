<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml\Event;

use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory as EventCollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Ticket\Controller\Adminhtml\Event as EventController;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Magenest\Ticket\Controller\Adminhtml\Event
 */
class MassDelete extends EventController
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param FileFactory $fileFactory
     * @param Filter $filter
     * @param EventCollectionFactory $collectionFactory
     * @param \Magento\Catalog\Model\ProductFactory $_productFactory
     */
    public function __construct(
        Context $context,
        EventFactory $eventFactory,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        FileFactory $fileFactory,
        Filter $filter,
        EventCollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ProductFactory $_productFactory
    ) {
        $this->_productFactory = $_productFactory;
        parent::__construct($context, $eventFactory, $coreRegistry, $resultPageFactory, $fileFactory, $filter,
            $collectionFactory);
    }

    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $delete = 0;
        foreach ($collection as $item) {
            $product = $this->_productFactory->create()->load($item->getProductId());
            $product->delete();
            /** @var \Magenest\Ticket\Model\Event $item */
            $item->delete();
            $delete++;
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $delete));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
