<?php
/**
 * Copyright Â© 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Cart;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Ticket\Helper\Event;

/**
 * Class Add
 * @package Magenest\Ticket\Controller\Cart
 */
class Add extends \Magento\Checkout\Controller\Cart\Add
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\Ticket\Model\EventoptionFactory
     */
    protected $option;

    /**
     * @var $optionType \Magenest\Ticket\Model\EventoptionTypeFactory
     */
    protected $optionType;

    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\EventDateFactory
     */
    protected $date;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var $_eventHelper Event
     */
    protected $_eventHelper;

    /**
     * Add constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param Event $event
     * @param Registry $coreRegistry
     * @param \Magenest\Ticket\Model\EventoptionFactory $optionFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\EventDateFactory $dateFactory
     * @param \Magenest\Ticket\Model\EventSessionFactory $sessionFactory
     * @param \Magenest\Ticket\Model\EventoptionTypeFactory $eventoptionTypeFactory
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        Event $event,
        Registry $coreRegistry,
        \Magenest\Ticket\Model\EventoptionFactory $optionFactory,
        \Magenest\Ticket\Model\EventLocationFactory $locationFactory,
        \Magenest\Ticket\Model\EventDateFactory $dateFactory,
        \Magenest\Ticket\Model\EventSessionFactory $sessionFactory,
        \Magenest\Ticket\Model\EventoptionTypeFactory $eventoptionTypeFactory
    ) {

        $this->optionType = $eventoptionTypeFactory;
        $this->location = $locationFactory;
        $this->date = $dateFactory;
        $this->session = $sessionFactory;
        $this->option = $optionFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_eventHelper = $event;
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $productRepository
        );
    }

    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        $productInformation = $this->_initProduct();
        if ($productInformation->getTypeId() === 'ticket') {
            $this->productRepository->cleanCache();
            $this->cart->getQuote()->setTotalsCollectedFlag(false);
            foreach ($this->cart->getQuote()->getAllAddresses() as $address) {
                $address->unsetData('cached_items_all');
            }
            $ticketParams = $params;
            try {
                if (isset($ticketParams['qty'])) {
                    $filter = new \Zend_Filter_LocalizedToNormalized(
                        [
                            'locale' => $this->_objectManager->get(
                                \Magento\Framework\Locale\ResolverInterface::class
                            )->getLocale()
                        ]
                    );
                    $ticketParams['qty'] = $filter->filter($ticketParams['qty']);
                }

                $product = clone $this->_initProduct();
                if (@$ticketParams['additional_options']) {
                    $customerOptions = $this->generateCustomerOptions($ticketParams);
                } else {
                    $this->messageManager->addNotice(__('You need to choose options for your item.'));
                    return $this->goBack($product->getUrlInStore());
                }
                if (!empty($customerOptions)) {
                    $product->addCustomOption('additional_options', $customerOptions);
                }
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    return $this->goBack();
                }

                $this->cart->addProduct($product, $ticketParams);
                if (!empty($related)) {
                    $this->cart->addProductsByIds($related);
                }


                /**
                 * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
                 */
                $this->_eventManager->dispatch(
                    'checkout_cart_add_product_complete',
                    ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
                );

                if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                    if (!$this->cart->getQuote()->getHasError()) {
                        $message = __(
                            'You added %1 to your shopping cart.',
                            $product->getName()
                        );
                        $this->messageManager->addSuccessMessage($message);
                    }
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->messageManager->addNotice(
                        $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
                    );
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $this->messageManager->addError(
                            $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($message)
                        );
                    }
                }

                $url = $this->_checkoutSession->getRedirectUrl(true);

                if (!$url) {
                    $cartUrl = $this->_objectManager->get(\Magento\Checkout\Helper\Cart::class)->getCartUrl();
                    $url = $this->_redirect->getRedirectUrl($cartUrl);
                }

                return $this->goBack($url);
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
                return $this->goBack();
            }


            $this->cart->save();
            if (isset($product)) {
                return $this->goBack(null, $product);
            } else {
                return $this->goBack(null, $this->_initProduct());
            }
        } else {
            try {
                if (isset($params['qty'])) {
                    $filter = new \Zend_Filter_LocalizedToNormalized(
                        [
                            'locale' => $this->_objectManager->get(
                                \Magento\Framework\Locale\ResolverInterface::class
                            )->getLocale()
                        ]
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }

                $product = $this->_initProduct();
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    return $this->goBack();
                }

                $this->cart->addProduct($product, $params);
                if (!empty($related)) {
                    $this->cart->addProductsByIds(explode(',', $related));
                }

                $this->cart->save();

                /**
                 * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
                 */
                $this->_eventManager->dispatch(
                    'checkout_cart_add_product_complete',
                    ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
                );

                if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                    if (!$this->cart->getQuote()->getHasError()) {
                        $message = __(
                            'You added %1 to your shopping cart.',
                            $product->getName()
                        );
                        $this->messageManager->addSuccessMessage($message);
                    }
                    return $this->goBack(null, $product);
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->messageManager->addNotice(
                        $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
                    );
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $this->messageManager->addError(
                            $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($message)
                        );
                    }
                }

                $url = $this->_checkoutSession->getRedirectUrl(true);

                if (!$url) {
                    $cartUrl = $this->_objectManager->get(\Magento\Checkout\Helper\Cart::class)->getCartUrl();
                    $url = $this->_redirect->getRedirectUrl($cartUrl);
                }

                return $this->goBack($url);
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
                return $this->goBack();
            }
        }
    }

//    }

    /**
     * @param $ticketParams
     * @param $productId
     * @return string
     */
    private function generateCustomerOptions($ticketParams)
    {
        $options = @$ticketParams['additional_options'];
        $additionalOptions = [];
        if (!empty($ticketParams['additional_options']['ticket_price'])) {
            if (isset($options['checkbox']) && !empty($options['checkbox'])) {
                foreach ($options['checkbox'] as $checkboxId => $optionsCheckbox) {
                    $title = $this->getOptionTitle($checkboxId);
                    $optionsCheckboxTitle = [];
                    foreach ($optionsCheckbox as $id => $optionCheckbox) {
                        $optionsCheckboxTitle[] = $this->getValueOption($id);
                    }
                    $additionalOptions[] = array(
                        'label' => $title,
                        'value' => implode(",", $optionsCheckboxTitle),
                    );
                }
            }
            if (isset($options['radio']) && !empty($options['radio'])) {
                foreach ($options['radio'] as $radioId => $radioOptions) {
                    /** @var  \Magenest\Ticket\Model\Eventoption $optionModel */
                    $title = $this->getOptionTitle($radioId);
                    $value = explode("_", $radioOptions);
                    $additionalOptions[] = array(
                        'label' => $title,
                        'value' => $this->getValueOption((int)$value[1]),
                    );
                }
            }
            if (isset($options['dropdown']) && !empty($options['dropdown'])) {
                foreach ($options['dropdown'] as $dropdownId => $dropdownOptions) {
                    if ($dropdownOptions == "") {
                        continue;
                    }
                    /** @var  \Magenest\Ticket\Model\Eventoption $optionModel */
                    $title = $this->getOptionTitle($dropdownId);
                    $value = explode("_", $dropdownOptions);
                    $additionalOptions[] = array(
                        'label' => $title,
                        'value' => $this->getValueOption((int)$value[1]),
                    );
                }
            }
        }
        $locationId = '';
        $dateId = '';
        $sessionId = '';
        if (isset($options['single']) && !empty($options['single'])) {
            if (!empty($options['single']['ticket_location'])) {
                $locationId = $options['single']['ticket_location'];
            }
            if (!empty($options['single']['ticket_date'])) {
                $dateId = $options['single']['ticket_date'];
            }
            if (!empty($options['single']['ticket_session'])) {
                $sessionId = $options['single']['ticket_session'];
            }
        } else {
            if (isset($options['ticket_location']) && !empty($options['ticket_location'])) {
                $locationId = $options['ticket_location'];
            }

            if (isset($options['ticket_date']) && !empty($options['ticket_date'])) {
                $dateId = $options['ticket_date'];
            }

            if (isset($options['ticket_session']) && !empty($options['ticket_session'])) {
                $sessionId = $options['ticket_session'];
            }
        }

        if (!empty($locationId)) {
            $location = $this->location->create()->load($locationId);

            $additionalOptions[] = [
                'label' => 'Location',
                'value' => $location->getLocationTitle()
            ];
        }
        if (!empty($dateId)) {
            $date = $this->date->create()->load($dateId);
            $start = 'no limit';
            $end = 'no limit';

            if (!empty($date->getDateStart())) {
                $dateStart = substr($date->getDateStart(), 0, 10);
                $start = trim(date("d-m-Y", strtotime($dateStart)));
            }
            if (!empty($date->getDateEnd())) {
                $dateEnd = substr($date->getDateEnd(), 0, 10);
                $end = trim(date("d-m-Y", strtotime($dateEnd)));
            }
            if (substr($date->getDateStart(), 0, 10) == substr($date->getDateEnd(), 0, 10)){
                $dateStr = $start;
            } else {
                $dateStr = 'From ' . $start . ' To ' . $end;
            }
            $additionalOptions[] = [
                'label' => 'Date',
                'value' => $dateStr
            ];
        }
        if (!empty($sessionId)) {
            $session = $this->session->create()->load($sessionId);
            $startTime = '';
            if (!empty($session->getStartTime())) {
                $startTime = 'Start: ' . $session->getStartTime() . ' ';
            }
            $endTime = '';
            if (!empty($session->getEndTime())) {
                $endTime = 'End: ' . $session->getEndTime();
            }
            $time = $startTime . $endTime;
            $additionalOptions[] = [
                'label' => 'Time',
                'value' => $time
            ];
        }

        if (!empty($ticketParams['ticket_info'])) {
            $additionalOptions[] = [
                'label' => 'info',
                'value' => $ticketParams['ticket_info']
            ];
        }
        $check = class_exists('Magento\Framework\Serialize\Serializer\Json');
        if ($check) {
            return json_encode($additionalOptions);
        } else {
            return serialize($additionalOptions);
        }
    }

    /**
     * getTitle of option
     * @param $productId
     * @param $option
     * @return mixed
     */
    public function getOptionTitle($id)
    {
        return $this->option->create()->load($id)->getOptionTitle();
    }

    /**
     * @param $id
     * @return string
     */
    public function getValueOption($id)
    {
        return $this->optionType->create()->load($id)->getTitle();
    }
}
