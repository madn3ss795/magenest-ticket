<?php
/**
 * Created by PhpStorm.
 * User: namnt
 * Date: 7/19/18
 * Time: 2:21 PM
 */

namespace Magenest\Ticket\Controller\Order;

/**
 * Class Reorder
 * @package Magenest\Ticket\Controller\Order
 */
class Reorder extends \Magento\Sales\Controller\Order\Reorder
{

    public function execute()
    {
        $result = $this->orderLoader->load($this->_request);
        if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
            return $result;
        }
        $order = $this->_coreRegistry->registry('current_order');
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /* @var $cart \Magento\Checkout\Model\Cart */
        $cart = $this->_objectManager->get(\Magento\Checkout\Model\Cart::class);
        $items = $order->getItemsCollection();
        $orderHasTicket = false;
        foreach ($items as $item) {
            try {
                if ($item->getProductType() == "ticket") {
                    $orderHasTicket = true;
                    continue;
                }
                $cart->addOrderItem($item);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_objectManager->get(\Magento\Checkout\Model\Session::class)->getUseNotice(true)) {
                    $this->messageManager->addNotice($e->getMessage());
                } else {
                    $this->messageManager->addError($e->getMessage());
                }
                return $resultRedirect->setPath('*/*/history');
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
                return $resultRedirect->setPath('checkout/cart');
            }
        }
        if ($orderHasTicket) {
            $this->messageManager->addNotice("Cannot add product event type to Reorder Cart.");
        }
        $cart->save();

        return $resultRedirect->setPath('checkout/cart');
    }
}
