<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Product;

/**
 * Class Register
 * @package Magenest\Ticket\Block\Product
 */
class Register extends \Magento\Catalog\Block\Product\View
{
    /**
     * @return bool
     */
    public function allowRegister()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $event = $objectManager->create('\Magenest\Ticket\Model\EventFactory');
        $context = $objectManager->create('\Magento\Catalog\Block\Product\Context');
        $product = $context->getRegistry()->registry('current_product');
        $event = $event->create()->getCollection()->addFieldToFilter('product_id', $product->getId())->getFirstItem();
        if ($event->getEventId()) {
            if ($event->getAllowRegister() == 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getCustomerWellcome()
    {
        if ($this->checkCustomerLogin()) {
            return "Hi, " . $this->getCustomerEmail() . "  Not you? <a href='" . $this->getUrl('customer/account/logout') . "'>Sign Out</a>";
        }

        return "";
    }

    /**
     * check whether customer login
     */
    public function checkCustomerLogin()
    {
        $customerSession = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn()) {
            return $customerSession->getCustomer();
        }

        return null;
    }

    /**
     * get firstname
     * @return string
     */
    public function getCustomerFirstName()
    {
        $customer = $this->checkCustomerLogin();

        return $customer ? $customer->getFirstname() : "";
    }

    /***
     * @return string
     */
    public function getCustomerLastName()
    {
        $customer = $this->checkCustomerLogin();

        return $customer ? $customer->getLastname() : "";    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        $customer = $this->checkCustomerLogin();

        return $customer ? $customer->getEmail() : "";
    }

    /**
     * @return bool
     */
    public function isRegisterRequired()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $event = $objectManager->create('\Magenest\Ticket\Model\EventFactory');
        $context = $objectManager->create('\Magento\Catalog\Block\Product\Context');
        $product = $context->getRegistry()->registry('current_product');
        $event = $event->create()->getCollection()->addFieldToFilter('product_id', $product->getId())->getFirstItem();
        if ($event->getEventId()) {
            if ($event->getIsRegisterRequired() == 1) {
                return true;
            }
        }

        return false;
    }
}
