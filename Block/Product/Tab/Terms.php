<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 11/04/2018
 * Time: 18:29
 */

namespace Magenest\Ticket\Block\Product\Tab;


class Terms extends \Magento\Catalog\Block\Product\View\Description
{

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    public $_eventFactory;

    /**
     * Terms constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        array $data = []
    ) {
        $this->_eventFactory = $eventFactory;
        parent::__construct($context, $registry, $data);
    }

    public function getTerms()
    {
        $productId = $this->getRequest()->getParams();
        $event = $this->_eventFactory->create()->loadByProductId($productId);
        if ($event->getId()) {
            return $event->getTerm();
        }

        return null;
    }
}
