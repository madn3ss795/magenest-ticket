<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Order\Item\Renderer;

/**
 * Class DefaultRenderer
 * @package Magenest\Ticket\Block\Order\Item\Renderer
 */
class DefaultRenderer extends \Magento\Sales\Block\Order\Item\Renderer\DefaultRenderer
{
    /**
     * @var \Magenest\Ticket\Helper\Information
     */
    protected $information;

    /**
     * DefaultRenderer constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory
     * @param \Magenest\Ticket\Helper\Information $information
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory,
        \Magenest\Ticket\Helper\Information $information,
        array $data
    ) {
        $this->information = $information;
        parent::__construct($context, $string, $productOptionFactory, $data);
    }

    /**
     * @param $options
     * @return array
     */
    public function getDataTicket($options)
    {
        $data = $this->information->getAll($options);
        $info = $this->information->getDataTicket($data);

        return $info;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOption($id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $optionType = $objectManager->get('Magenest\Ticket\Model\Eventoption')->load($id);
        return $optionType->getOptionTitle();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOptionType($id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $optionType = $objectManager->get('Magenest\Ticket\Model\EventoptionType')->load($id);
        return $optionType->getTitle();
    }
}
