<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Order;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Ticket\Model\ResourceModel\Ticket\CollectionFactory as TicketCollectionFactory;
use Magenest\Ticket\Helper\Information;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class History
 *
 * @package Magenest\Ticket\Block\Order
 */
class History extends Template
{
    /**
     * Google Map API key
     */
    const XML_PATH_GOOGLE_MAP_API_KEY = 'event_ticket/general_config/google_api_key';

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/history.phtml';

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_ticketCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var string
     */
    protected $tickets;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * History constructor.
     * @param Context $context
     * @param TicketCollectionFactory $ticketCollectionFactory
     * @param CustomerSession $customerSession
     * @param Information $information
     * @param array $data
     */
    public function __construct(
        Context $context,
        TicketCollectionFactory $ticketCollectionFactory,
        CustomerSession $customerSession,
        Information $information,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->information = $information;
        $this->_ticketCollectionFactory = $ticketCollectionFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Tickets'));
    }

    /**
     * Get Ticket Collection
     *
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getTickets()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->tickets) {
            $this->tickets = $this->_ticketCollectionFactory->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'customer_id',
                $customerId
            )->setOrder(
                'main_table.event_id',
                'desc'
            )->setOrder(
                'main_table.created_at',
                'desc'
            );
            $eventTable = $this->_ticketCollectionFactory->create()->getTable('magenest_ticket_event');
            $this->tickets->getSelect()->joinLeft(
                ['event' => $eventTable],
                'main_table.event_id = event.event_id ',
                ['*']
            );
        }

        return $this->tickets;
    }

    /**
     * Prepare layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getTickets()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'ticket.order.history.pager'
            )->setCollection(
                $this->getTickets()
            );
            $this->setChild('pager', $pager);
            $this->getTickets()->load();
        }

        return $this;
    }

    /**
     * Get Google Map Api key
     *
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_API_KEY);
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param object $ticket
     * @return string
     */
    public function getViewUrl($ticket)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $ticket->getOrderId()]);
    }

    /**
     * @param object $ticket
     * @return string
     */
    public function getPrintTicketUrl($ticket)
    {
        return $this->getUrl('ticket/order/pdfticket', ['ticket_id' => $ticket->getId()]);
    }

    /**
     * @param $option
     * @return array
     */
    public function getDataTicket($option)
    {
        $array = unserialize($option);
        $data = $this->information->getDataTicket($array);

        return $data;
    }

    public function getProductUrl($productId)
    {
        $product = $this->_productFactory->create()->load($productId);

        if ($product->getData()) {
            return $product->getProductUrl();
        }

        return "#";
    }
}
