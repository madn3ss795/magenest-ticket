<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Adminhtml\Product\Edit\Tab\Options\Type;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Type\Select as ProductOptionsSelect;

/**
 * Class Select
 * @package Magenest\Ticket\Block\Adminhtml\Product\Edit\Tab\Options\Type
 */
class Select extends ProductOptionsSelect
{
    /**
     * @var string
     */
    protected $_template = 'catalog/product/edit/options/type/select.phtml';
}
