<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 12/04/2018
 * Time: 14:14
 */

namespace Magenest\Ticket\Block\Adminhtml\Product\Edit\Element;

use Magento\Framework\View\Element\AbstractBlock;

/**
 * Class CreatePDF
 * @package Magenest\Ticket\Block\Adminhtml\Product\Edit\Element
 */
class CreatePDF extends AbstractBlock
{

    protected $formName = 'product_form';

    /**
     * @return string
     */
    public function toHtml()
    {
        return $this->getElementHtml();
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {
        $html = $this->getContentHtml();

        return $html;
    }

    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {
        /* @var $content \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Gallery\Content */
        $content = $this->getChildBlock('content');
        if (!$content) {
            $content = $this->getLayout()->createBlock('Magenest\Ticket\Block\Adminhtml\Product\Edit\Element\Content');
        }
        $content->setId($this->getHtmlId() . '_content')->setElement($this);
        $content->setFormName($this->formName);

        return $content->toHtml();
    }
}
