<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 12/04/2018
 * Time: 14:22
 */

namespace Magenest\Ticket\Block\Adminhtml\Product\Edit\Element;

/**
 * Class Content
 * @package Magenest\Ticket\Block\Adminhtml\Product\Edit\Element
 */
class Content extends \Magento\Backend\Block\Widget
{

    protected $_template = 'catalog/product/edit/elements/createpdf.phtml';

    public function createNewTemplate()
    {
        return $this->getUrl('magenest_ticket/template/addNew');
    }
}
