<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Adminhtml\Event\Edit;

use Magento\Framework\View\Element\AbstractBlock;

/**
 * Class AssignSummary
 * @package Magenest\Ticket\Block\Adminhtml\Event\Edit
 */
class AssignSummary extends AbstractBlock
{

    protected $formName = 'product_form';


    /**
     * @return string
     */
    public function toHtml()
    {
        return $this->getElementHtml();
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {

        $html = $this->getContentHtml();
        return $html;
    }

    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {

        $content = $this->getChildBlock('content');
        if (!$content) {
            $content = $this->getLayout()->createBlock('Magenest\Ticket\Block\Adminhtml\Event\Edit\Tab\Summary');
        }
        $content->setId($this->getHtmlId() . '_content')->setElement($this);
        $content->setFormName($this->formName);

        return $content->toHtml();
    }
}
