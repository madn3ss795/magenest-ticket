<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Adminhtml\Template\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveAndContinue
 * @package Magenest\Ticket\Block\Adminhtml\Template\Edit\Button
 */
class PrintPDF extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Print PDF'),
            'on_click' => sprintf("location.href = '%s';", $this->getPrintUrl()),
            'sort_order' => 80,
        ];

        return $data;
    }

    protected function getPrintUrl()
    {
        if (@$this->request->getParam("template_id")) {
            return $this->getUrl('*/*/printTicket', ['id' => $this->request->getParam("template_id")]);
        }

        return $this->getUrl('*/*/');
    }
}
