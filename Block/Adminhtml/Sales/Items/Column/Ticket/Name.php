<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Adminhtml\Sales\Items\Column\Ticket;

/**
 * Class Name
 * @package Magenest\Ticket\Block\Adminhtml\Sales\Items\Column\Ticket
 */
class Name extends \Magento\Sales\Block\Adminhtml\Items\Column\Name
{
    /**
     * @var \Magenest\Ticket\Helper\Information
     */
    protected $information;

    /**
     * @var \Magenest\Ticket\Helper\Ticket
     */
    protected $_ticketHelper;

    /**
     * Name constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product\OptionFactory $optionFactory
     * @param \Magenest\Ticket\Helper\Ticket $ticketHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Magenest\Ticket\Helper\Ticket $ticketHelper,
        array $data
    ) {
        $this->_ticketHelper = $ticketHelper;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $optionFactory, $data);
    }

    /**
     * @param $options
     * @return array
     */
    public function getInfoTicket($options)
    {
        return $this->_ticketHelper->getInfoTicket($options);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOptionType($id)
    {
        return $this->_ticketHelper->getTitleOptionType($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTitleOption($id)
    {
        return $this->_ticketHelper->getTitleOption($id);
    }
}
