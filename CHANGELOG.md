# CHANGELOG #

### 101.1.0 ### 2018-1-20

* Fix bugs
* Refactor code
* For customer register : customer receive ticket
* Create default template

### 101.0.5 ### 2017-10-11

* Fix bugs
* Refactor code

### 1.0.3 ### 2017-08-05

* Fix bug : add to wishlist, remove item on minicart, exception
* Enabled/Disabled date-time
* For admin print ticket and resend mail for customer
* Change calculation coordinate

### 1.0.2 ### 2016-11-30

* Fix bug
* Add multiple location
* Add multiple Date
* Add multiple Time
* Change configuration : email, calculate
* Change email template
* Change database
* Change frontend

### 1.0.1 ### 2016-08-29

* Fix bug
* Fix Access Control List in Backend

### 1.0.0 ###

* Add new product type: Event Ticket Product
* Add new tab: Event Booking in product edit/new page panel
* Add coordinates, background in PDF Template
* Add attachted PDF file in email 
* Add option
* Deployment instructions
